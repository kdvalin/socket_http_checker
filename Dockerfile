FROM python:slim

COPY . /app/

#install deps
RUN pip3 install -r /app/requirements.txt
RUN pip3 install gunicorn

EXPOSE 8000

ENTRYPOINT [ "gunicorn", "--chdir", "app", "server:app", "-w 2", "-b 0.0.0.0:8000" ]