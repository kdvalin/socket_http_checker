# How to run (Development)

1. Set up venv
    1. `python3 -m venv venv`
    2. `source venv/bin/activate`
2. Install dependencies (`pip3 install -r requirements.txt`)
3. Run `FLASK_APP=server flask run`

# How to run (Production/Gunicorn)
`gunicorn 'server:app`

Add options to taste

# Monitoring endpoint

`HTTP GET /<protocol>/<host>/<port>`