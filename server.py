#!/usr/bin/env python3

import socket
from flask import Flask, abort

UDP=socket.SOCK_DGRAM
TCP=socket.SOCK_STREAM

app = Flask(__name__)

def netcat(host, port, content, type):
    s = socket.socket(socket.AF_INET, type)
    s.connect((host, int(port)))
    s.sendall(content)
    s.shutdown(socket.SHUT_WR)
    while True:
        s.settimeout(0.5)
        data = None
        try:
            data = s.recv(4096)
        except TimeoutError:
            pass
        if not data:
            break
    s.close()

@app.get('/<proto>/<host>/<port>')
def do_GET(proto, host, port):
    type = TCP
    if proto.lower() == "udp":
        type = UDP

    response = 200
    try:
        netcat(host, port, b"hello", type)
    except socket.gaierror:
        abort(404)
    except ConnectionRefusedError:
        abort(404)
    return ""
